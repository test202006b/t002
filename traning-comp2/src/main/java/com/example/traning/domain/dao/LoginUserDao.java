package com.example.traning.domain.dao;

import com.example.traning.domain.entity.LoginUser;
import com.example.traning.domain.mapper.LoginMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoginUserDao {

    @Autowired
	LoginMapper mapper;

	public LoginUser findUser(String username) {
        return mapper.findUser(username);
    }

}