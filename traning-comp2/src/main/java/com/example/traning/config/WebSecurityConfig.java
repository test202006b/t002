package com.example.traning.config;

import com.example.traning.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 認証認可設定
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
 

    @Autowired
    private UserService userService;
 
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
	BCryptPasswordEncoder bcpe = new BCryptPasswordEncoder();
        return bcpe;
    }

    /**
     * 認証、認可の設定
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        // 静的リソースに対するアクセスはセキュリティ設定を無視する
        web.ignoring().antMatchers("/css/**");
    }

     /**
     * 認証、認可の設定
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            // TODO　認可設定
            .authorizeRequests()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login/loginForm") // ログインのビュー
                .loginProcessingUrl("/login") //認証処理が実行される
                .usernameParameter("username") 
                .passwordParameter("userpassword")
                .successForwardUrl("/top")  // ログイン成功時の遷移先
                .failureUrl("/login?error") // ログイン失敗時の遷移先＋パラメタ
                .permitAll()
                .and()
            .logout()
                .logoutUrl("/logout") // ログアウトのビュー
                .logoutSuccessUrl("/login?logout") // ログアウト成功時の遷移先 + パラメータ
                .permitAll();
    }
 
    /**
     * 認証処理部分
     * 
     */

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception{
    	auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }
}