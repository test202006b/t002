
package com.example.traning.service;

import java.util.ArrayList;
import java.util.List;

import com.example.traning.domain.dao.LoginUserDao;
import com.example.traning.domain.entity.LoginUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 
 
@Service
@Transactional
public class UserService implements UserDetailsService {
	@Autowired
	private LoginUserDao  LoginUserDao ;
 
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LoginUser user = LoginUserDao.findUser(username);
		System.out.println("LOGINUSER INSTANCE");

		if (user == null) {
			throw new UsernameNotFoundException("userName" + username + "was not found in the database");
		}
 
		List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
		GrantedAuthority authority = new SimpleGrantedAuthority("USER");
		grantList.add(authority);
 
		// BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
 
		UserDetails userDetails = (UserDetails) new User(user.getUserId(), user.getUserPassword(),
				grantList);
		return userDetails;
	}
}